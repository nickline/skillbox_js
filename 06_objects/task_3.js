const filter = (usersArr, targetKey, targetValue) => {
  filtredUsersArr = []

  for (let user of usersArr) {
    if (user[targetKey] === targetValue) {
      filtredUsersArr.push(user)
    }
  }
  return filtredUsersArr
}

let objects = [
  { name: "Василий", surname: "Васильев" },
  { name: "Иван", surname: "Иванов" },
  { name: "Пётр", surname: "Петров" },
]
let result = filter(objects, "name", "Иван")
console.log(result)
