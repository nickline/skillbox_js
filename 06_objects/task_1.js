const getOlderUser = (firstUser, secondUser) => {
  return firstUser.age > secondUser.age ? firstUser.name : secondUser.name
}

let user1 = {
  name: "Игорь",
  age: 17,
}
let user2 = {
  name: "Оля",
  age: 21,
}
// Вызов созданной функции
let result = getOlderUser(user1, user2)
console.log(result)
