const getOlderUserArray = (usersArr) => {
  if (!usersArr.length) {
    return -1
  }

  let olderUserName = ""
  let olderUserAge = -Infinity

  for (let { name, age } of usersArr) {
    if (age > olderUserAge) {
      olderUserName = name
      olderUserAge = age
    }
  }
  return olderUserName
}

let allUsers = [
  { name: "Валя", age: 11 },
  { name: "Таня", age: 24 },
  { name: "Рома", age: 21 },
  { name: "Надя", age: 34 },
  { name: "Антон", age: 7 },
]

console.log(getOlderUserArray(allUsers))

console.log(getOlderUserArray([]))
