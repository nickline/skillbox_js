let start = -50
let end = 70

let rangeNumbers = Math.abs(start - end)
let firstRandomNum = Math.round(
  Math.min(start, end) + Math.random() * rangeNumbers
)

let secondRandomNum = Math.round(
  Math.min(start, end) + Math.random() * rangeNumbers
)
console.log("a =", firstRandomNum, "| b =", secondRandomNum)

console.log("a === b is", firstRandomNum === secondRandomNum)
