let firstNum = 13.123456789
let secondNum = 2.123
let precision = 5

firstNumNormalized = Math.round((firstNum % 1) * Math.pow(10, precision))
secondNumNormalized = Math.round((secondNum % 1) * Math.pow(10, precision))

console.log("a =", firstNumNormalized, "| b =", secondNumNormalized)

console.log("a > b is", firstNum > secondNum)
console.log("a < b is", firstNum < secondNum)
console.log("a >= b is", firstNum >= secondNum)
console.log("a <= b is", firstNum <= secondNum)
console.log("a === b is", firstNum === secondNum)
console.log("a !=== b is", firstNum !== secondNum)
