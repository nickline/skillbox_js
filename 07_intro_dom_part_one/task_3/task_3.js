const createStudentsList = (userListArr) => {
  let ul = document.createElement("ul")
  for ({ name, age } of userListArr) {
    let li = document.createElement("li")
    let h2 = document.createElement("h2")
    let span = document.createElement("span")

    h2.textContent = name
    span.innerHTML = `<span>Возраст: ${age} лет</span>`

    h2.className = "userName"
    span.className = "userAge"
    li.append(h2)
    li.append(span)
    ul.append(li)
  }

  document.body.append(ul)
}

let allStudents = [
  { name: "Валя", age: 11 },
  { name: "Таня", age: 24 },
  { name: "Рома", age: 21 },
  { name: "Надя", age: 34 },
  { name: "Антон", age: 7 },
]
createStudentsList(allStudents)
