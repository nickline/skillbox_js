;(() => {
  /**
   * Генерирует массив пар целых чисел.
   * @param {number} numPairs Количество чисел в массиве.
   * @returns Массив целых чисел.
   */
  const generatePairs = (numPairs) => {
    let pairs = []
    for (let i = 1; i <= numPairs; i++) {
      let digit = i % 10
      digit = digit > 0 ? digit : 1

      pairs.push(digit, digit)
    }
    return pairs
  }

  /**
   * Перемешивает элементы массива в случайном порядке.
   * @param {Array} arr Массив целых чисел.
   * @returns Исходный массив.
   */
  const shuffleArray = (arr) => {
    for (let i = arr.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1))
      ;[arr[i], arr[j]] = [arr[j], arr[i]]
    }
    return arr
  }

  /**
   * Создает и перемешивает массив пар целых чисел.
   * @param {number} numPairs Количество чисел в массиве.
   * @returns Массив целых чисел.
   */
  const createShuffledPairs = (numPairs) => {
    return shuffleArray(generatePairs(numPairs))
  }

  document.addEventListener("DOMContentLoaded", () => {
    const gameBoard = document.getElementById("gameBoard")
    const resetButton = document.getElementById("resetButton")
    const settingsForm = document.getElementById("settingsForm")
    const timerDisplay = document.getElementById("timer")
    let firstCard = null
    let secondCard = null
    let lockBoard = false
    let matchedCount = 0
    let timer = null
    let timeLeft = 60

    /**
     * Запускает таймер на одну минуту и обновляет отображение таймера каждую секунду.
     * Если время истекает, игра заканчивается и появляется сообщение.
     */
    const startTimer = () => {
      timeLeft = 60
      updateTimerDisplay()
      timer = setInterval(() => {
        timeLeft--
        updateTimerDisplay()
        if (timeLeft <= 0) {
          clearInterval(timer)
          alert("Время вышло! Игра окончена.")
          resetBoardState()
        }
      }, 1000)
    }

    /**
     * Обновляет отображение таймера в формате MM:SS.
     */
    const updateTimerDisplay = () => {
      const minutes = Math.floor(timeLeft / 60)
        .toString()
        .padStart(2, "0")
      const seconds = (timeLeft % 60).toString().padStart(2, "0")
      timerDisplay.innerText = `${minutes}:${seconds}`
    }

    /**
     * Сбрасывает состояние игрового поля и останавливает таймер.
     */
    const resetBoardState = () => {
      clearInterval(timer)
      firstCard = null
      secondCard = null
      lockBoard = false
      matchedCount = 0
    }

    /**
     * Инициализирует игру, создавая игровое поле заданного размера и запуская таймер.
     * @param {number} gridSize - Размер игрового поля (по умолчанию 4x4).
     */
    const initializeGame = (gridSize = 4) => {
      gameBoard.innerHTML = ""
      resetBoardState()
      gameBoard.style.gridTemplateColumns = `repeat(${gridSize}, 1fr)`
      const numPairs = (gridSize * gridSize) / 2
      const cardValues = createShuffledPairs(numPairs)
      cardValues.forEach((value) => {
        const card = document.createElement("div")
        card.classList.add("card", "hidden")
        card.dataset.value = value
        card.innerText = value
        card.addEventListener("click", handleCardClick)
        gameBoard.appendChild(card)
      })
      startTimer()
    }

    /**
     * Обрабатывает клик по карточке, открывает её и проверяет на совпадение.
     * @param {Event} event - Событие клика.
     */
    const handleCardClick = (event) => {
      if (lockBoard) return

      const clickedCard = event.target
      if (clickedCard === firstCard) return

      clickedCard.classList.remove("hidden")

      if (!firstCard) {
        firstCard = clickedCard
      } else {
        secondCard = clickedCard
        checkForMatch()
      }
    }

    /**
     * Проверяет совпадение пары карточек. Если пара совпала, оставляет их открытыми,
     * в противном случае закрывает карточки через секунду.
     */
    const checkForMatch = () => {
      lockBoard = true
      const isMatch = firstCard.dataset.value === secondCard.dataset.value

      if (isMatch) {
        firstCard.classList.add("matched")
        secondCard.classList.add("matched")
        matchedCount += 2
        resetBoard()
      } else {
        setTimeout(() => {
          firstCard.classList.add("hidden")
          secondCard.classList.add("hidden")
          resetBoard()
        }, 1000)
      }
      if (matchedCount === gameBoard.children.length) {
        clearInterval(timer)
        alert("Победа! Все пары найдены!")
      }
    }

    /**
     * Сбрасывает выбранные карточки и разблокирует доску.
     */
    const resetBoard = () => {
      ;[firstCard, secondCard] = [null, null]
      lockBoard = false
    }

    /**
     * Проверяет допустимость размера сетки.
     * @param {number} size - Размер сетки.
     * @returns {boolean} - Возвращает true, если размер недопустим, иначе false.
     */
    const isValidGridSize = (size) => {
      return isNaN(size) || size < 2 || size > 10 || size % 2 !== 0
    }

    settingsForm.addEventListener("submit", (event) => {
      event.preventDefault()
      const gridSizeInput = document.getElementById("gridSize").value
      let gridSize = parseInt(gridSizeInput, 10)
      if (isValidGridSize(gridSize)) {
        gridSize = 4
      }
      initializeGame(gridSize)
    })

    resetButton.addEventListener("click", () => {
      const gridSizeInput = document.getElementById("gridSize").value
      let gridSize = parseInt(gridSizeInput, 10)
      if (isValidGridSize(gridSize)) {
        gridSize = 4
      }
      initializeGame(gridSize)
    })

    initializeGame()
  })
})()
