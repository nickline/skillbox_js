const filter = (emailsArr, blacklistEmailsArr) => {
  const whitelistEmailsArr = []
  for (let email of emailsArr) {
    if (!blacklistEmailsArr.includes(email)) {
      whitelistEmailsArr.push(email)
    }
  }

  return whitelistEmailsArr
}

// Массив с почтовыми адресами:
let whiteList = [
  "my-email@gmail.ru",
  "jsfunc@mail.ru",
  "annavkmail@vk.ru",
  "fullname@skill.ru",
  "goodday@day.ru",
]
// Массив с почтовыми адресами в чёрном списке:
let blackList = ["jsfunc@mail.ru", "goodday@day.ru"]
console.log(filter(whiteList, blackList))
