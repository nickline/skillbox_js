let userName = "Brendan"
let userSurname = "Eich"

let userNameNormalized = userName.charAt(0).toUpperCase() + userName.slice(1)
let userSurnameNormalized =
  userSurname.charAt(0).toUpperCase() + userSurname.slice(1)

console.log(
  userName === userNameNormalized && userSurname === userSurnameNormalized
    ? "Имя осталось без изменений"
    : "Имя было преобразовано"
)
