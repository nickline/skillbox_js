;(function () {
  /**
   * Создает заголовок h2 для приложения.
   * @param {string} titleValue
   * @returns Строка.
   */
  const createAppTitle = (titleValue) => {
    let title = document.createElement("h2")
    title.innerHTML = titleValue

    return title
  }

  /**
   * Создает форму для TODO.
   * @returns Объект с полями: form, inputField и btn.
   */
  const createTodoItemForm = () => {
    let form = document.createElement("form")
    let inputField = document.createElement("input")
    let btnWrapper = document.createElement("div")
    let btn = document.createElement("button")

    form.classList.add("input-group", "mb-3")
    inputField.classList.add("form-control")
    inputField.placeholder = "Название TODO"
    btnWrapper.classList.add("input-group-append")
    btn.classList.add("btn", "btn-success")
    btn.textContent = "Добавить"
    btn.disabled = true

    inputField.addEventListener("input", () => {
      btn.disabled = !inputField.value.trim()
    })

    btnWrapper.append(btn)
    form.append(inputField)
    form.append(btnWrapper)

    return {
      form,
      inputField,
      btn,
    }
  }

  /**
   * Создает тег ul.
   * @returns Элемент ul.
   */
  const createTodoList = () => {
    let todoList = document.createElement("ul")
    todoList.classList.add("list-group")

    return todoList
  }

  /**
   * Создает элементы TODO, с кнопками "Выполнено" и "Удалить".
   * @param {object} param0 Объект с полями: name, done и id.
   * @returns Объект с полями: todoItem, btnComplete и btnDelete.
   */
  const createTodoItem = ({ name, done, id }) => {
    let todoItem = document.createElement("li")
    let btnWrapper = document.createElement("div")
    let btnComplete = document.createElement("button")
    let btnDelete = document.createElement("button")

    todoItem.classList.add(
      "list-group-item",
      "d-flex",
      "justify-content-between",
      "align-items-center"
    )
    todoItem.id = id
    btnWrapper.classList.add("btn-group", "btn-group-sm")
    btnComplete.classList.add("btn", "btn-info")
    btnDelete.classList.add("btn", "btn-warning")

    todoItem.textContent = name
    btnComplete.textContent = "Выполнено"
    btnDelete.textContent = "Удалить"

    btnWrapper.append(btnComplete)
    btnWrapper.append(btnDelete)
    todoItem.append(btnWrapper)

    return {
      todoItem,
      btnComplete,
      btnDelete,
    }
  }

  /**
   * Устаналивает значения в локальную память.
   * @param {string} key Ключ.
   * @param {object} localData Данные для хранения.
   */
  const setToLocalStorage = (key, localData) => {
    localStorage.setItem(key, JSON.stringify(localData))
  }

  /**
   * Получает данные по ключу из локальной памяти.
   * @param {string} key
   * @returns Объект с полями: name, done и id. Пустой массив, если в памяти пусто.
   */
  const getFromLocalStorage = (key) => {
    const localData = localStorage.getItem(key)
    return localData ? JSON.parse(localData) : []
  }

  let data = []
  /**
   * Создает формы TODO для каждой страницы.
   * @param {string} todoContainerID Идентификатор нужного контейнера на странице.
   * @param {string} todoGroupHeader Заголовок для страницы.
   * @param {string} listName  Ключ объекта.
   */
  const createTodoApp = (
    todoContainerID,
    todoGroupHeader = "TODO List",
    listName = "default"
  ) => {
    let todoApp = document.getElementById(todoContainerID)
    let todoAppTitle = createAppTitle(todoGroupHeader)
    let todoItemForm = createTodoItemForm()
    let todoList = createTodoList()

    todoApp.append(todoAppTitle)
    todoApp.append(todoItemForm.form)
    todoApp.append(todoList)

    data = getFromLocalStorage(listName)
    data.forEach((item) => {
      let todo = createTodoItem(item)

      todo.btnComplete.addEventListener("click", () => {
        todo.todoItem.classList.toggle("list-group-item-success")
        item.done = !item.done
        setToLocalStorage(listName, data)
      })

      todo.btnDelete.addEventListener("click", () => {
        if (confirm("Вы хотите удалить?")) {
          todo.todoItem.remove()
          data = data.filter((elem) => elem.id !== item.id)
          setToLocalStorage(listName, data)
        }
      })

      todoList.append(todo.todoItem)
    })

    todoItemForm.form.addEventListener("submit", (event) => {
      event.preventDefault()

      if (!todoItemForm.inputField.value) {
        return
      }

      let newTodo = {
        id: Math.round(1 + Math.random() * 999),
        name: todoItemForm.inputField.value,
        done: false,
      }

      let todo = createTodoItem(newTodo)

      todo.btnComplete.addEventListener("click", () => {
        todo.todoItem.classList.toggle("list-group-item-success")
        newTodo.done = !newTodo.done
        setToLocalStorage(listName, data)
      })

      todo.btnDelete.addEventListener("click", () => {
        if (confirm("Вы хотите удалить?")) {
          todo.todoItem.remove()
          data = data.filter((item) => item.id !== newTodo.id)
          setToLocalStorage(listName, data)
        }
      })

      todoList.append(todo.todoItem)
      data.push(newTodo)
      setToLocalStorage(listName, data)
      todoItemForm.inputField.value = ""
      todoItemForm.btn.disabled = true
    })
  }
  window.createTodoApp = createTodoApp
})()
