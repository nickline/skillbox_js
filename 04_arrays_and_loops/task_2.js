let count = 5

let arr = []

for (let i = 0; i < count; i++) {
  arr[i] = i + 1
}

console.log(arr)

for (let i = arr.length - 1; i >= 0; i--) {
  let randomIndex = Math.floor(Math.random() * i)
  ;[arr[i], arr[randomIndex]] = [arr[randomIndex], arr[i]]
}
console.log(arr)
