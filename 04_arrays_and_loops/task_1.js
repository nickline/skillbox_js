let startRange = 2
let endRange = 5
let count = 20

let arr = []

for (let i = 0; i < 100; i++) {
  arr[i] = Math.round(
    Math.min(startRange, endRange) +
      Math.random() * Math.abs(startRange - endRange)
  )
}
console.log(arr)
