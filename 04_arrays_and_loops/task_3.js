let arr = [2, 3, -4, 4, 5, -4, 1]
let target = -4

let isFound = false
for (let i = 0; i < arr.length; i++) {
  if (arr[i] === target) {
    console.log(i) // 2
    isFound = true
    break
  }
}

if (!isFound) {
  console.log(`Элемент "${target}" не найден`)
}
